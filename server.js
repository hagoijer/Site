#!/bin/env node
var express = require('express'),
	app = express(),

	// Mongodb setup
	mongodb = require('mongodb'),
	BSON = mongodb.BSONPure,
	mongohost = process.env.OPENSHIFT_MONGODB_DB_HOST || "127.0.0.1",
	mongoport = parseInt(process.env.OPENSHIFT_MONGODB_DB_PORT, 10) || 27017,
	dbServer = new mongodb.Server(mongohost, mongoport),
	db = new mongodb.Db('horecaro', dbServer, {auto_reconnect: true}),
	dbUser = process.env.OPENSHIFT_MONGODB_DB_USERNAME || "horecaro",
	dbPass = process.env.OPENSHIFT_MONGODB_DB_PASSWORD || "horecaro",

	//
	res = null,
	req = null,
	err = null,
	result = null,

	//statuses
	newAtClient = 1 << 0,
	newAtServer = 1 << 1,
	changedAtClient = 1 << 2,
	changedAtServer = 1 << 3,
	deleteAtClient = 1 << 4,
	deleteAtServer = 1 << 5,
	syncedAtClient = 1 << 6,
	syncedAtSever = 1 << 7,

	//Verbose or not?    
	beVerbose = true;

function verbose(msg) {
	if (beVerbose) {
		console.log(msg);
	}
}

function handleRes(err, result, res) {
	if (err) {
		res.json(err);
	} else {
		res.json(result);
	}
}

db.open(function (err, db) {
	if (err) {
		throw err;
	}

	var authdb = (dbUser == "horecaro") ? "horecaro" : "admin";

	db.authenticate(dbUser, dbPass, {authdb: authdb}, function (err, res) {
		if (err) {
			throw err;
		}
	});
});

app.configure(function () {
	app.set('PORT', process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
	app.set('IPADDR', process.env.IP || process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");
	app.use(express.compress());
	app.use(express.static(__dirname + '/public'));
	app.use(express.bodyParser());
});

//Employee api
app.get('/api/employees', function (req, res) {
	var criteria = {},
		options = {},
		itemsPerPage = 10;

	if(req.query.cat) {
		criteria.cat = parseInt(req.query.cat, 10);
	}
	if (req.params.itemsPerPage) {
		itemsPerPage = req.params.itemsPerPage;
		options.limit = req.params.itemsPerPage;
	}
	if(req.params.page) {
		options.skip = (req.params.page - 1) * itemsPerPage;
	}

	db.collection('employees', function(err, collection){
		collection.find(criteria, options).toArray(function(err, result){
			handleRes(err, result, res);
		});
	});
});

app.get('/api/employees/:id', function (req, res) {
	var criteria = {};

	if(req.params.id) {
		criteria._id = new BSON.ObjectID(req.params.id);
	}


	db.collection('employees', function(err, collection){
		collection.find(criteria).toArray(function(err, result){
			verbose(err);
			verbose(result);
			handleRes(err, result, res);
		});
	});
});

app.post('/api/employees', function (req, res) {
	if (req.get('Content-Type').indexOf('application/json') !== -1){
		var employee = req.body;
		verbose(employee);

		db.collection('employees', function(err, collection){
			collection.insert(employee, function(err, result){
				res.statusCode = 201;
				handleRes(err, result, res);
			});
		});
	} else {
		res.statusCode = 415;
		handleRes(null, null, res);
	}
});

app.put('/api/employees/:id', function (req, res) {
	if (req.get('Content-Type').indexOf('application/json') !== -1){
		var employeeId = new BSON.ObjectID(req.param.id),
		updates;

		db.collection('employees', function(err, collection) {
			//TODO: Check input before updating!
			collection.update( {"_id": employeeId}, updates, {upsert: true}, handleRes(err, result, res));
		});
	} else {
		res.statusCode = 415;
		handleRes(null, null, res);
	}
});

app.del('/api/employees/:id', function (req, res) {
	var employeeId = new BSON.ObjectID(req.params.id);
	db.collection('employees', function(err, collection){
		collection.remove({_id: employeeId}, {safe:1}, function(err, result) {
			if (result === 1){
				res.statusCode = 204;
			}
			handleRes(err, result, res);
		});
	});
});

//Timeline api
app.get('/api/timelines', function (req, res) {
	var criteria = {},
		options = {},
		itemsPerPage = 10;

	if(req.query.date) {
		criteria.date = req.query.date;
	}
	if(req.query.employeeId) {
		criteria.employeeId = req.query.employeeId;
	}
	if (req.query.itemsPerPage) {
		itemsPerPage = req.query.itemsPerPage;
		options.limit = itemsPerPage;
	}
	if(req.query.page) {
		options.skip = (req.query.page -1 ) * itemsPerPage;
	}
	if(req.query.sort) {
		options.sort = req.query.sort;
	}

	db.collection('timelines', function(err, collection) {
		collection.find(criteria, options, function(err, timelines) {
			timelines.toArray( function(err, result) {
				handleRes(err, result, res);
			});
		});
	});
});

app.post('/api/timelines', function (req, res) {
	if (req.get('Content-Type').indexOf('application/json') !== -1){
		var timeline = req.body;
		verbose(timeline);

		db.collection('timelines', function(err, collection){
			collection.insert(timeline, function(err, result){
				res.statusCode = 201;
				handleRes(err, result, res);
			});
		});
	} else {
		res.statusCode = 415;
		handleRes(null, null, res);
	}
});

app.put('/api/timelines', function (req, res) {
	if (req.get('Content-Type').indexOf('application/json') !== -1){
		var timeline = req.body;
		verbose(timeline);

		db.collection('timelines', function(err, collection){
			collection.update( {"_id": timeline.id},
					{$set : timeline}, //Check input!
					{upsert: true},
					function(err, result) {
						handleRes(err, result, res);
					});
		});
	} else {
		res.statusCode = 415;
		handleRes(null, null, res);
	}
});

app.del('/api/timelines/:id', function (req, res) {
	var timelineId = new BSON.ObjectID(req.params.id);
	db.collection('timelines', function(err, collection) {
		collection.remove({_id: timelineId }, {safe:1}, function(err, result) {
			if (result === 1){
				res.statusCode = 204;
			}
			handleRes(err, result, res);
		});
	});
});

//Login api
app.post('/login', function (req, res) {
	verbose(req.body.username + " " + req.body.password);

	res.statusCode = 200;
	res.end('succes');
});

app.listen(app.get('PORT'), app.get('IPADDR'), function () {
	console.log("Express server listening on " + app.get('IPADDR') + ":" + app.get('PORT'));
});