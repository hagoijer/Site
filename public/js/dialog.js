/* Author:
	Henrik Goijer
	
	Require: main.js, dialog.css
*/

var Dialog = (function() {

	var dialogId = "dialogContainer";

	// This function creates a new dialog based on a json object
	function _new(callback, dialogJSON) {
		createNode("div", { id : dialogId, class: "dialogContainer"}, function(dialogContainer) {
			createNode("div", { class: "dialogCenter"}, function(centerDiv) {
				createNode("div", { id : "dialogDiv", class: "dialog"}, function(dialogDiv){
					createNode("form", {id : "dialogForm", method: "POST", ajaxify: 1, 'accept-charset': "UTF-8", action: callback}, function(form) {
						createNode("fieldset", { id : "dialogFieldset" }, function(fieldset) {
							//Initial dom
							if(dialogJSON.title) {
								createNode("legend", {id : "titleField", class: 'titlefield'}, function(title){
									title.innerHTML = dialogJSON.title;
								}, fieldset);
							}

							if(dialogJSON.msg) {
								createNode("p", {id : "msgField", class: 'msgfield'}, function(msg) {
									msg.innerHTML = dialogJSON.msg;
								}, fieldset);
							}
							//Fill in the dialog
							//Add fields if given
							if(dialogJSON.fields) {
								addFields(dialogJSON.fields, fieldset);
							}
							//Add buttons if given
							if(dialogJSON.buttons) {
								addButtons(dialogJSON.buttons, fieldset);
							}
							//Project dialog
						}, form);
					}, dialogDiv);
				}, centerDiv);
			}, dialogContainer);

		if( dialogJSON.modal === 1 ) {
			dialogContainer.classList.add("overlay");
		}

		$("container").appendChild(dialogContainer);
		});

		//Close dialog on pressing escape button
		doc.addEventListener('keypress', closeOneEsc(e), false);

		//Close dialog on clicking the modal
		doc.addEventListener('click', closeOnClick(e), false);
	}
    
    function closeOnClick(e){
		var elm = e.target;

		while (elm && elm.id !== dialogId) {
			elm = elm.parentNode;
			if (elm.id == dialogId) {
				return;
			}
		}

		Dialog.close();
	}
    
    function closeOneEsc(e){
        e = window.event || e;

		if(e.keyCode == 27) {
			Dialog.close();
		}
	}

	function addFields(fields, parentNode) {
		createNode("div", { id : "fields"}, function(fieldsDOM){
			for(var field in fields) {
				createNode("label", { 'for' : fields[field].name }, function(label) {
					label.innerHTML = fields[field].name + ":";
				}, fieldsDOM);

				createNode("input", { id : fields[field].name, name : fields[field].name, type: fields[field].type}, null, fieldsDOM);

				createNode('br', null , null, fieldsDOM);
			}
		}, parentNode);
	}

	function addButtons(buttons, parentNode){
		createNode("div", { id: "buttons" }, function(buttonsDOM) {
			for(var button in buttons) {
				createNode((buttons[button].type == "submit" ? "input" : "button"), {
					id : buttons[button].name,
					name : buttons[button].name,
					type : buttons[button].type}, function(buttonDOM) {

					if(buttons[button].type == "close"){
						buttonDOM.setAttribute("onClick", "Dialog.close();");
					}

					//Proper valaue assignment. Differs for button and input element
					if (buttonDOM.getAttribute("type") == "submit") buttonDOM.setAttribute("value", buttons[button].name);
					if (buttonDOM.getAttribute("type") == "close") buttonDOM.innerHTML = buttons[button].name;
				}, buttonsDOM);
			}
		}, parentNode);
	}

	function _close() {
        doc.removeEventListener('keypress', closeOneEsc(e), false);
        doc.removeEventListener('click', closeOnClick(e), false);
		removeById(dialogId);
	}

	return {
		'new' : function(callback, dialogJSON){
			_new(callback, dialogJSON);
		},

		'close' : function() {
			_close();
		}
	};
})();