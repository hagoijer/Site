/* Author:
	Henrik Goijer
	
	Require: main.js
*/
var Timetable = (function(){
	"use strict";

	var today = new Date(),
		d = today.getUTCDate(),
		m = today.getUTCMonth() + 1,
		y = today.getUTCFullYear(),
		selectedDate = (y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d)),
		timelines = [],

        date = new Date(selectedDate + ' 09:00:00'),
        nineAMTimestamp = date.getTime(),

		//statuses
		newAtClient = 1 << 0,
		newAtServer = 1 << 1,
		changedAtClient = 1 << 2,
		changedAtServer = 1 << 3,
		deleteAtClient = 1 << 4,
		deleteAtServer = 1 << 5,
		syncedAtClient = 1 << 6,
		syncedAtSever = 1 << 7;

	function newTimelineId() {
		var id = genId();
		timelines[timelines.length] = { status: newAtClient, date: selectedDate, clientId: id};
		verbose("Added timeline with id: " + id);
		return id;
	}
    
    function newTimeline(timeline) {
        
        var id = (timeline) ? (timeline.clientId) ? timeline.clientId : timeline.clientId = genId() : newTimelineId();
        
        var employeeId = timeline && timeline.employeeId || null;
        
        $('container', function(container){
            
            var dialogJSON = {title:"Employee","msg":"Please provide or ajust timeline data","buttons":[{"name":"Login","type":"submit"},{"name":"Close","type":"close"}],"fields":[{"name":"Name","type":"text"},{"name":"Password","type":"password"}]};
            
            
            createNode("div", { id : "dialogContainer", class: "dialogContainer"}, function(dialogContainer) {
                createNode("div", { class: "dialogCenter"}, function(centerDiv) {
                    createNode("div", { id : "dialogDiv", class: "dialog"}, function(dialogDiv){
                        createNode("fieldset", { id : "dialogFieldset" }, function(fieldset) {
                            //Initial dom
                                if(dialogJSON.title) {
                                	createNode("legend", {id : "titleField", class: 'titlefield'}, function(title){
                                		title.innerHTML = dialogJSON.title;
                                	}, fieldset);
                                }
                
                                if(dialogJSON.msg) {
                                	createNode("p", {id : "msgField", class: 'msgfield'}, function(msg) {
                                		msg.innerHTML = dialogJSON.msg;
                                	}, fieldset);
                                }
                                //Fill in the dialog
                                //Add fields if given
                                
                            createNode('select', {id: 'employee-' + id}, function(select) {
                                //If no or null employeeId is given set this option on selected
                                select.options[select.options.length] = new Option("", "no-employee-selected", ((!employeeId) ? true : false));

                                Api.get('/api/employees', function(err, result) {
                                    if(err) {
                                        verbose('Unable to fetch list of employees. ' + err);
                                        return;
                                    } else {
                                        for (var employee in result){
                                            select.options[select.options.length] = new Option(result[employee].name, result[employee]._id, (employeeId == result[employee]._id ? true : false));
                                            getByTag('select', function(selects){
                                                var i = selects.length;

                                                while(i--) {
                                                    if (selects[i].value.indexOf(result[employee]._id) === 0){
                                                        select.options[select.options.length -1].setAttribute('disabled', true);
                                                        break;
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }, fieldset);

                                function newTimeSelector(id, type, time, cb) {
                                	createNode('select', {id: type + 'Time-' + id}, function(select) {

                                        var timestamp = nineAMTimestamp;
                                        
                                        select.options[select.options.length] = new Option("", "no-start-time-selected", (!time ? true : false));
                                        
                                        
                                        for (var k = 9; k <= 27; k++) {
                                            var hour = (k < 24 ? k : k -24);
                                            
                                            for( var x = 1; x <=2; x++) {
                                                var halfhour = (x % 2 === 0 ? "30" : "00"),
                                                    value = hour + ":" + halfhour; 

                                                select.options[select.options.length] = new Option(value, timestamp, (parseInt(time,10) === k ? true : false));
                                                timestamp += 18000000;    
                                            }
                                    
                                			
                                        }
                                
                                		if (cb) {
                                			cb(select);
                                		} else {
                                			return select;
                                		}
                                	});
                                }
                                
                                fieldset.appendChild()
                                fieldset.appendChild(newTimeSelector(id, start, startTime));
                                fieldset.appendChild(newTimeSelector(id, stop, stopTime));
                                
                                
                                if(dialogJSON.fields) {
                                	addFields(dialogJSON.fields, fieldset);
                                }
                                //Add buttons if given
                                if(dialogJSON.buttons) {
                                	addButtons(dialogJSON.buttons, fieldset);
                                }
                                //Project dialog
                        }, dialogDiv);
                    }, centerDiv);
                }, dialogContainer);

                if( dialogJSON.modal === 1 ) {
                    dialogContainer.classList.add("overlay");
                }

            }, container);
        });
    
    }

	function addTimelineToTable(timeline) {
		   //Describe calculation
        var emptyPixels = (timeline.startTime - nineAMTimestamp) / 90000,
            busyPixels = (timeline.stopTime - timeline.startTime) / 90000;
            verbose(nineAMTimestamp);
            verbose(emptyPixels);
            
		var id = (timeline) ? (timeline.clientId) ? timeline.clientId : timeline.clientId = genId() : newTimelineId();
        
        createNode('tr', { 'id': 'timeline-' + id }, function(row){
			//Employee
			createNode('td', {id: 'employee-' + id}, function(cell){
					cell.innerHTML = timeline.employeeName;
			}, row);

			//Start time
			createNode('td', {id: 'stopTime-' + id}, function(cell){
                    var h = new Date(timeline.startTime).getHours(),
                        m = new Date(timeline.startTime).getMinutes(),
                        time = h + ":" + (m <= 9 ? '0' + m : m);
					cell.innerHTML = time;
			}, row);

			//duration time
			createNode('td', {id: 'duration-' + id}, function(cell){
				cell.innerHTML = (timeline.stopTime - timeline.startTime)/60/60/1000;
			}, row);

    		//Grant line
			createNode('td', null, function(cell){
                
				createNode('img', {id: 'wait-' + id, src: 'img/shim.gif', width: emptyPixels + "px", height: "10px"}, null, cell);
                createNode('img', {id: 'busy-' + id, src: 'img/busy.gif', width: busyPixels + "px", height: "10px"}, null, cell);
			}, row);

			//Edit timeline button
			createNode('td', null, function(cell){
				createNode('button', {
					id: 'editTimeline-' + id,
					class: 'button edit'
				}, null, cell);
			}, row);

			//Del timeline button
			createNode('td', null, function(cell){
				createNode('button', { id: 'removeTimeline-' + id, class: 'button'}, function (button){
    			    createNode('span', {class: "icon del"}, null, button);
				}, cell);
			}, row);
		}, $('timetablebody'));
		setEvent('editTimeline-' + id, 'click', function() {
			editTimeline(id);
		});
		setEvent('removeTimeline-' + id, 'click', function() {
			removeTimeline(id);
		});
	}

	function cleanTimetable() {
		getByTag('tr', function(tableRows){
			var i = tableRows.length;

			while(i--) {
				if (tableRows[i].id.indexOf('timeline-') === 0) {
					removeById(tableRows[i].id);
				}
			}
		});
	}

	function removeTimeline(id) {
        verbose("Removing timeline : " + id + " from the timetable");
		//Remove from ui
		removeById('timeline-' + id);

		getIndexByProperty(timelines, 'clientId', id, function(timeline){
			if (timelines[timeline]._id !== undefined) {
				var serverId = timelines[timeline]._id;
				verbose('Removing time: ' + serverId + ' from server');
				Api.del('/api/timelines/' + serverId, function(err, result){
					if(err) {
						verbose("Removing timeline with serverId: " + serverId + " from db failed.");
					} else {
						verbose("Succesfully removed timeline with serverId: " + serverId + " from db.");
					}
				});
			}
			//Remove timeline from client timetable
			timelines.splice(timeline, 1);
		});
		verbose("Timeline " + id + " removed from the timetable");
	}

	function editTimeline (id) {
		getIndexByProperty(timelines, 'clientId', id, function(timeline){
            newTimeline(timeline);
		});
	}
    
    function updateTimeline(timeline) {
        var id = timeline.clientId,
            date = new Date(timeline.date + ' 09:00:00'),
            nineAMTimestamp = date.getTime(),
            //Describe calculation
            emptyPixels = (timeline.startTime - nineAMTimestamp) / 90000,
            busyPixels = (timeline.stopTime - timeline.startTime) / 90000;
        
        $('employee-' + id, function(employeeCell){
            employeeCell.innerHTML = timeline.employeeName;
        });

        $('startTime-' + id, function(startTimeCell) {
            var h = new Date(timeline.startTime).getHours(),
                m = new Date(timeline.startTime).getMinutes(),
                startTime = h + ":" + (m <= 9 ? '0' + m : m);
            startTimeCell.innerHTML = startTime;
        });
        
        $('duration-' + id, function(durationCell) {
            durationCell.innerHTML = (timeline.stopTime - timeline.startTime)/60/60/1000;
        });
        
        $('wait' + id, function(wait) {
            wait.width = emptyPixels;
        });
        
        $('busy' + id, function(busy) {
            busy.width = busyPixels;
        });
    }

	function commitTimelineChanges(timelines){ //TODO: Eventually change to sync between client and server data
		Api.put("/api/timelines", function(err, result){
			if (err) {
				verbose("Sending changes failed: " + err);
			} else {
				var responses = result.responses;
				for (var response in responses) {
					if (responses[response].status & syncedAtSever) {
						if (timelines[response]._id === null && responses[response]._id) {
							timelines[response]._id = responses[response]._id;
							verbose('Assinged timeline id: ' + responses[response]._id + ' to timeline ' + timelines[response]);
							timelines[response].status = responses[response].status;
						}
					}
					if (responses[response].status & changedAtServer) {
						verbose('Changed at server, please do something!');
					}
				}
			}
		}, timelines);
	}

	function loadTimetableHeader() {
		createNode('article', null, function(article){
			createNode('table', {id: 'timetable', class: 'timetable'}, function(table){
				createNode('thead', null, function(thead){
                    createNode('tr', null, function(tr){
                        createNode('th', {colspan: 7}, function(th){ th.innerHTML = "Grant chart"}, tr);
                    }, thead);
					createNode('tr', null, function(tr){
						createNode('th', {class: "grantHeader"}, function(th){ th.innerHTML = "Name"; }, tr);
						createNode('th', {class: "grantHeader"}, function(th){ th.innerHTML = "Start time"; }, tr);
						createNode('th', {class: "grantHeader"}, function(th){ th.innerHTML = "Duration"; }, tr);
                        createNode('th', {class: "grantHeader"}, function(th){ 
                            createNode('tr', {class: "noMargin noPadding"}, function(tr){
                                createNode('th', {class: "grantHeader noMargin noPadding", colspan: 19}, function(th){ th.innerHTML = "Grant indicator"}, tr);
                            }, th);
                            createNode('tr', {style: "width: 760px",class: "noMargin noPadding"}, function(tr){
                                for (var k = 9; k <= 27; k++) {
                                    var hour = (k < 24) ? k : k - 24;
                                    createNode('td', {class: "grantHeader grantHeaderTime noMargin noPadding"}, function(td){ td.innerHTML = hour + ":00"}, tr);
                                }
                            }, th);
                        }, tr);
						createNode('th', {class: "grantHeader"}, function(th){ th.innerHTML = "Edit"; }, tr);
						createNode('th', {class: "grantHeader"}, function(th){ th.innerHTML = "Delete"; }, tr);
					}, thead);
				}, table);
				createNode('tbody', {id: 'timetablebody'}, null, table);
			}, article);
		}, $('maincontent'));
        
        addTimelineToTable({date: "2013-06-26",employeeId: "519139dc1715992284ee51f2", employeeName : "Henrik Goijer", startTime: 1372240800000, stopTime: 1372248000000,_id: "51a13afa5ec28e0b56000001"});
        addTimelineToTable({date: "2013-06-26",employeeId: "519139dc1715992284ee51f3", employeeName : "Lucas Goijer", startTime: 1372244400000, stopTime: 1372273200000,_id: "51a13afa5ec28e0b56000002"});
        addTimelineToTable({date: "2013-06-26",employeeId: "519139dc1715992284ee51f4", employeeName : "Tom Goijer", startTime: 1372231800000, stopTime: 1372244400000,_id: "51a13afa5ec28e0b56000003"});
	}

	function addButtons() {
		$('buttons', function(buttons){
				createNode('input', {id: 'dateEntry', type: 'date'}, function(input) {
                    input.value = selectedDate;    
				}, buttons);
				createNode('button', { id: 'addEmployee',
					class: 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary'
				}, function(button){
					createNode('span', {class: 'ui-button-icon-primary ui-icon add'}, null, button);
					createNode('span', {class: 'ui-button-text'}, function (span){
						span.innerHTML = "Add employee";
					}, button);
				}, buttons);
				createNode('button', { id: 'saveChanges',
					class: 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary'
				}, function(button){
					createNode('span', {class: 'ui-button-icon-primary ui-icon save'}, null, button);
					createNode('span', {class: 'ui-button-text'}, function (span){
						span.innerHTML = "Save timetable";
					}, button);
				}, buttons);
				createNode('button', { id: 'cleanTimetable',
					class: 'button'
				}, function(button){
					createNode('span', {class: 'del'}, null, button);
					createNode('span', {class: 'ui-button-text'}, function (span){
						span.innerHTML = "Clean timetable";
					}, button);
				}, buttons);
		});
	}

	function assignButtonEvents() {
		setEvent('dateEntry', 'change', function() {
			$('dateEntry', function(dateEntry) {
				var oldDate = selectedDate;
				selectedDate = dateEntry.value;
				if (selectedDate !== oldDate) {
					commitTimelineChanges(timelines);
					cleanTimetable();
					loadTimelinesByDate(selectedDate);
				}
			});
		});

		setEvent('addEmployee', 'click', function() {
			newTimeline();
		});

		setEvent('saveChanges', 'click', function() {
			commitTimelineChanges(timelines);
		});

		setEvent('cleanTimetable', 'click', function() {
			cleanTimetable();
		});
	}

	function loadTimelinesByDate(date) {
		Api.get('/api/timelines?date=' + date, function(err, serverTimelines){
			if (err) {
				verbose('Unable to fetch timelines for date ' + date + ': ' + err);
			} else {
				verbose(serverTimelines);
				for(var serverTimeline in serverTimelines){
					timelines[timelines.length] = serverTimelines[serverTimeline];
					addTimelineToTable(timelines[serverTimeline]);
				}
			}
		});
	}

	return {
		init: function () {
			loadTimetableHeader();
			addButtons();
			assignButtonEvents();
			loadTimelinesByDate(selectedDate);
		}
	};
})();


// //Catch and handle changing values
// 		setEvent('employee-' + id, 'change', function() {
// 			$('employee-' + id, function(select){
// 				if (select.value == "no-employee-selected") {
// 					alert("Select an employee for timeline " + id);
// 				} else {
// 					getByTag('select', function(selects){
// 						var i = selects.length,
// 							oldval = timelines[id].employeeId;

// 						while(i--) {
// 							if (selects[i].id.indexOf('employee-') === 0 && selects[i].id.indexOf('employee-' + id) === -1) {
// 								$(selects[i].id, function(employeeSelector){
// 									var employeeOptions = employeeSelector.options,
// 										j = employeeOptions.length;

// 									while(j--) {
// 										if (employeeOptions[j].value.indexOf(select.value) === 0) {
// 											employeeOptions[j].setAttribute('disabled', true);
// 										}
// 										if (employeeOptions[j].value.indexOf(oldval) === 0) {
// 											employeeOptions[j].removeAttribute('disabled');
// 										}
// 									}
// 								});
// 							}
// 						}
// 					});

// 					timelines[id].employeeId = employeeId = select.value;
// 					if(employeeId) timelines[id].status |= changedAtClient;
// 					verbose(employeeId + " is assinged to timeline: " + id);
// 				}
// 			});
// 		});
// 		setEvent('startTime-' + id, 'change', function() {
// 			$('startTime-' + id, function(start) {
// 				$('stopTime-' + id, function(stop){
// 					var options = stop.options,
// 						i = options.length;

// 					while(i--) {
// 						options[i].removeAttribute('disabled');
// 						if (options[i].value <= start.value) {
// 							options[i].setAttribute('disabled', true);
// 						}
// 					}
// 				});
// 				timelines[id].startTime = startTime = start.value;
// 				if(startTime) timelines[id].status |= changedAtClient;
// 				verbose("Chanced start time for " + employeeId + " in " + startTime);
// 			});
// 		});
// 		setEvent('stopTime-' + id, 'change', function() {
// 			$('stopTime-' + id, function(stop){
// 				$('startTime-' + id, function(start){
// 					var options = start.options,
// 						i = options.length;

// 					while(i--) {
// 						options[i].removeAttribute('disabled');
// 						if (options[i].value >= stop.value) {
// 							options[i].setAttribute('disabled', true);
// 						}
// 					}
// 				});
// 				timelines[id].stopTime = stopTime = stop.value;
// 				if(stopTime) timelines[id].status |= changedAtClient;
// 				verbose("Chanced stop time for " + employeeId + " in " + stopTime);
// 			});
// 		});
		






		// $("employee-" + id, function(employeeSelector){
		// 	var selectedEmployee = employeeSelector.options[employeeSelector.selectedIndex].value;
		// 	getByTag('option', function(options){
		// 		var i = options.length;

		// 		while(i--) {
		// 			if (options[i].value.indexOf(selectedEmployee) === 0){
		// 				options[i].removeAttribute('disabled');
		// 			}
		// 		}
		// 	});
		// });
