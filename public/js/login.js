/* Author:
    Henrik Goijer
    
    Require: main.js, dialog.js
*/

var loginDialog = {
        "modal" : 1,
        "title" : "Login",
        "msg" : "Please provide valide login information to login",
        "buttons" : [
            {
                "name": "Login",
                "type": "submit"
            },
            {
                "name": "Close",
                "type": "close"
            }
        ],
        "fields" : [
            {
                "name": "Name",
                "type": "text"
            },
            {
                "name": "Password",
                "type": "password"
            }
        ]
    },
    dialog = new Dialog();

function setupUI() {
    verbose("New login dialog");
    dialog.new(loginDialog);
}