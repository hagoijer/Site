function newTimeline(id) {
    $('#timetable').append($('<div id=timeline-'+id+' />').addClass('timetablerule'));
    $('#timeline-'+id).append($('<div id=employee-selector />').addClass('employee-selector').append($('<select id=employee-'+id+' />')));
    $.getJSON("http://localhost/Site/ajax.php", {
        q: "employees"
       }, function(data){
           $.each(data.employees, function(i, item){
                $('#employee-'+id).append('<option value=employee-id-'+ item.id + '>'+ item.name +'</option>');
           });
    });
    //Create first 23 hour of the timeline
    for (var j = 0; j <= 23; j++){
        $('#timeline-'+id).append($('<div id='+j+'/>').addClass('tl v').droppable( {
            hoverClass: "dnd-state-hover",
            activeClass: "dnd-state-active",
            tolerance: "touch",
            drop: handleDrop,
            accept: '#employee-'+id+'-start, #employee-'+id+'-stop'
        }));
        $('#timeline-'+id).append($('<div />').addClass('tl h'));
    }
    //Add end of timeline (24th hour).
    $('#timeline-'+id).append($('<div id=24/>').addClass('tl v').droppable( {
            hoverClass: "dnd-state-hover",
            activeClass: "dnd-state-active",
            tolerance: "touch",
            accept: '#employee-'+id+'-stop',
            drop: handleDrop
        }));    
    $('#timeline-'+id).append($('<div />').addClass('clearfix'));
    //Make start and stop draggable
    $('#employee-'+id+'-start').add($('#employee-'+id+'-stop')).draggable({
            snap: "true",
            snapMode: "inner",
            snapTolerance: 20
        });
}


function handleDrop(event, ui){
    $(this).addClass('dnd-state-correct');
    ui.draggable.addClass('fixed');
    ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
    if (ui.draggable.hasClass('start')){ 
        startTime = $(this).attr('id');
    } else if (ui.draggable.hasClass('stop')) {
        stopTime = $(this).attr('id');
    }

    if (startTime !== 0 && stopTime !== 0){
        $('#log').html(stopTime - startTime);
    }

}


function Employee(cat){
    this.startTime = 0;
    this.stopTime = 0;
    this.date = 0;
    this.category = cat;
    
    
    this.createUI = function(){
        $('#employee-'+employee-id+'-start').add($('#employee-'+employee-id+'-stop')).draggable({
            snap: "true",
            snapMode: "inner",
            snapTolerance: 20
        });
    };
}

function _createRooster(){
$('#rooster').append($('<div id="timeline" />'));
    for ( var i = 10; i<= 30; i++){
        var hour ="";
        if (i < 24){
            hour = i; 
        } else {
            hour = i - 24;
        }
        $('#timeline').append($('<div />').addClass('hour').html(hour));
    }
    $('#timeline').append($('<div />').addClass('clearfix'));
    $('#rooster').append($('<div id="timetable" />'));
}
