/* 
    Author: Henrik Goijer
*/

// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
	(function() {
		var noop = function() {};
		var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'],
			length = methods.length,
			console = window.console = {};
		while (length--) {
			console[methods[length]] = noop;
		}
	}());
}

//Class to communicate with the api provided by server.js
//Sends and recieves JSON in body and command by url
var Api = {
	_xhr : function (){
		var http = false;
		//Use IE's ActiveX items to load the file.
		if(typeof ActiveXObject != 'undefined') {
			try {http = new ActiveXObject("Msxml2.XMLHTTP");}
			catch (e) {
				try {http = new ActiveXObject("Microsoft.XMLHTTP");}
				catch (E) {http = false;}
			}
		//If ActiveX is not available, use the XMLHttpRequest of Firefox/Mozilla etc. to load the document.
		} else if (window.XMLHttpRequest) {
			try {http = new XMLHttpRequest();}
			catch (e) {http = false;}
		}
		return http;
	},

	_send : function (type, url, cb, data) {
		var xhr = this._xhr(),
			requestTimeout = setTimeout(function() {xhr.abort(); cb(new Error("Aborted by a timeout"), "", xhr); }, 10000);

		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
				clearTimeout(requestTimeout);
				cb(xhr.status !== 200 ? new Error("Server response status is " + xhr.status) : false, JSON.parse(xhr.responseText), xhr);
			}
		};
		xhr.open(type, url, true);
		xhr.setRequestHeader("content-type", (type == "POST"||"PUT" ? "application/json; charset=utf-8" : "application/x-www-form-urlencoded"));
		if(data){
			xhr.setRequestHeader("content-length", data.length);
		}
		xhr.send((data ? JSON.stringify(data) : null));
	},

	get: function (url, callback){
		this._send("GET", url, callback);
	},
	put: function (url, callback, data){
		this._send("PUT", url, callback, data);
	},
	post: function (url, callback, data){
		this._send("POST", url, callback, data);
	},
	del: function (url, callback){
		this._send("DELETE", url, callback);
	},
	options: function (url, callback){
		this._send("OPTIONS", url, callback);
	}
};

function saveResponseToVar(response, variable){
	variable = response;
}

//Function for adding and removing node to/from the dom and attaching listeners
//Create new nodes by giving element name and attributes. Thanks to https://github.com/rgrove ' lazyload
function createNode(name, attrs, cb, parentNode) {
	var output = doc.createElement(name), attr;

	for (attr in attrs) {
		(attrs.hasOwnProperty(attr)) && output.setAttribute(attr, attrs[attr]);
	}
	(cb) && cb(output);
    (parentNode) && parentNode.appendChild(output);
    return output;
}

function removeById(id)
{
	$(id, function(elem){
		elem.parentNode.removeChild(elem);
	});
}

function $(elementId, cb) {
	var output = doc.getElementById(elementId);
	(cb) && cb(output);
	return output;
}

function getByTag(element, cb) {
	var output = doc.getElementsByTagName(element);
	(cb) && cb(output);
	return output;
}

function setEvent (elementId, event, cb) {
	var evn = "on"+event;
	$(elementId, function(elem){
		elem[evn] = cb;
	});
}


function getIndexByProperty (object, property, value, cb) {
	var output = null;
	verbose(object);
	for (var i in object) {
		verbose(object[i]);
		if (object[i].hasOwnProperty(property)) {
			verbose('object has property');
			if(object[i][property] == value){
				verbose('its a match made in heaven');
				output = i;
				break;
			} else {
				return new Error('property has not this value');
			}
		} else {
			return new Error('no such property');
		}
	}
	(cb) && cb(output);
	return output;
}

function genId() {
	return Math.random().toString(36).substr(2, 9);
}

var Content = (function(){

	var content = {
		timetable: function () { LazyLoad.load('Timetable', function(){
				Timetable.init();
			});
		},
		employees: function () { LazyLoad.load('Employees', function(){
				Employees.init();
			});
		}
	};

	function loadContent (href) {
		$('buttons', function(buttons){ buttons.innerHTML = "";});
		$('maincontent', function(main){ main.innerHTML = "";});
		(content[href]) && content[href]();
	}

	return {
		init: function (href) {
			loadContent(href);
		}
	};
})();